var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({extended: false});
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost/uhbank';
var db;

module.exports = (function(app){

    app.get('/', function(req,res){
        res.render('home');
    });

    app.get('/register', function(req,res){
        res.render('register');
    });

    app.get('/login', function(req,res){
        res.render('login');
    });

    app.post('/demo', urlencodedParser, function(req,res){
        MongoClient.connect(db.url,(err,database) => {
            const uhbankDB = database.db('uhbank')
            uhbankDB.collection('users').findOne({ name: req.body.name}, function(err, user){
                if(user === null){
                    res.end("Login invalid");
                }else if(user.name === req.body.name && user.pass === req.body.pass){
                    res.render('register', {profileData: user});
                }else {
                    console.log("Credentials wrong");
                    res.end("Login invalid");
                }
            });
        });
    });

    app.post('/registerToDb', urlencodedParser, function(req,res){
        var obj = JSON.stringify(req.body);
        var jsonObj = JSON.parse(obj);
        res.render('home', {loginData: req.body});
    });
});