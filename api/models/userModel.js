'use strict';
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

const schema = new Schema({
    nome:  { type: String, unique: true, required: true},
    senha: { type: String, required: true},
    email: { type: String, required: true},
    createdDate: { type: Date, default: Date.now}
});

schema.set('toJSON', { virtuals: true});

module.exports = mongoose.model('User', schema);