'use strict';
module.exports = function(app) {
    var r = require('../controllers/userController');

    app.route('/welcome')
        .get(r.list_all_user)
        .post(r.create_a_user);

    app.route('/welcome/:userId')
        .get(r.read_a_user)
        .put(r.update_a_user)
        .delete(r.delete_a_user);
};