var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

    //Render STATIC FILES
    var login = require('./api/controllers/loginController.js');
    app.set('view engine','ejs');
    app.use(express.static('./public'));
    login(app);

app.listen(port);

console.log('dashboard RESTful API server started on: ' + port);